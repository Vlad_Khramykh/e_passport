import 'package:flutter/cupertino.dart';

const kPrimaryColor = Color(0xFF0C9869);
const kTextColor = Color(0xFF3C4846);
const kBackgroundColor = Color(0XFFFFFFFF);

const double kDefaultPadding = 28;

const String ePassportMRZTemplate = 'IDBLRAA991286897576570A003PB6<\n' +
    '7012026M3010024BLR<<<<<<<<<<<8\n' +
    'GRISHKOVETS<<SERGEI<<<<<<<<<<<';

const String passportMRZTemplate =
    'P<BLRXXXXXXX<<YYYYYYYYYY<<<<<<<<<<<<<<<<<<<\n' +
        'SSPPPPPPPNTLDDDDDDDDDDDDDDDIIIIIIIIIIIIIIZZ';

const String ISO7816_READ_CARD_REQUEST = '1';
const String ISO7816_READ_CARD_RES_OK = '200';
const String ISO7816_READ_CARD_RES_ERROR = '400';
const String ISO7816_READ_CARD_RES_ERROR_MRZ_VALIDATE = '401';
const String ISO7816_READ_CARD_RES_ERROR_CHIP_AUTH = '402';
const String ISO7816_READ_CARD_RES_ERROR_PASSIVE_AUTH = '403';
const String ISO7816_READ_CARD_RES_ERROR_DATA_NOT_FOUND = '404';
const String ISO7816_READ_CARD_RES_ERROR_CERT_NOT_FOUND = '405';

const String MAIN_URL = "http://biometry.avest.by";
const String PORT = "8282";
const String REGISTRTION_URL = '/api/icao/v1/register';
const String OPENSESSION_URL = '/api/icao/v1/session';
const String CHEKING_URL = '/api/icao/v1/check';
