import 'dart:io';

import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Future alertWidget(title, message, context) {
  if (Platform.isIOS) {
    return showCupertinoDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(title),
        content: Text(message),
        actions: <Widget>[
          CupertinoDialogAction(
            isDefaultAction: true,
            child: Text('OK'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  } else {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[Text(message)],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }
}

void flushBar(
    BuildContext context, String title, String message, bool isError) {
  Flushbar(
    padding: EdgeInsets.all(15),
    margin: EdgeInsets.all(15),
    borderRadius: 8,
    backgroundGradient: LinearGradient(
      colors: !isError
          ? [Colors.green.shade800, Colors.greenAccent.shade700]
          : [Colors.red.shade800, Colors.redAccent.shade700],
      stops: [0.6, 1],
    ),
    boxShadows: [
      BoxShadow(
        color: Colors.black45,
        offset: Offset(3, 3),
        blurRadius: 3,
      ),
    ],
    shouldIconPulse: true,
    isDismissible: true,
    duration: new Duration(seconds: 5),
    dismissDirection: FlushbarDismissDirection.HORIZONTAL,
    forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
    title: title,
    message: message,
  )..show(context);
}

AppBar buildAppBar() {
  return AppBar(
    elevation: 0,
    title: const Text('E_Passport'),
    // leading: IconButton(
    //   onPressed: () {},
    // ),
  );
}
