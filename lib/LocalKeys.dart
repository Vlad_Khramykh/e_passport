class LocalKeys {
  static const String title = 'title';

  static final String mrzScannerPageTitle = 'mrz_page_title';
  static final String scanIdCard = 'scan_id_card';
  static final String scanPassport = 'scan_passport';
  static final String toRegistrationPageButton = "registration_page_button";
  static final String errorAlertTitle = 'alert.error_title';

  static final String enableNfc = 'nfc.enable';
  static final String nfcNotSupported = 'nfc.not_supported';
  static final String enableNfcForNextStateText = 'nfc.enable_for_next_step';
  static final String nfcAlertTitle = 'nfc.alert_title';

  static final String registrationRowText = "registration_text";
  static final String authenticationRowText = "authentication_text";
  static final String anticloneCheckingRowText = "anticlone_checking_text";

  static final String authenticateButtonText = 'authenticate';
  static final String authenticationPassedText = 'authentication.passed';
  static final String authenticationFailedText = 'authentication.failed';

  static const String awaitingForPermissions = 'awaiting_for_permissions';

  static const String cardReadSuccessAlertTitle = 'card.read_success_title';
  static const String cardReadSuccessAlertBody = 'card.read_success_text';
  static const String cardReadFailedAlertTitle = 'card.read_failed_title';

  static const String cardFormFirstName = 'card.form.firstname';
  static const String cardFormLastName = 'card.form.lastname';
  static const String cardFormNationality = 'card.form.nationality';
  static const String cardFormDocumentNumber = 'card.form.document_number';
  static const String cardFormPersonalNumber = 'card.form.personal_number';
}
