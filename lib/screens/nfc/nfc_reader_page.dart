import 'package:e_passport/screens/nfc/components/body.dart';
import 'package:flutter/material.dart';
import 'package:mrz_parser/mrz_parser.dart';

import '../../general.dart';

class NfcReaderPage extends StatefulWidget {
  final MRZResult mrzResult;
  NfcReaderPage({Key key, this.mrzResult}) : super(key: key);

  @override
  _NfcReaderPageState createState() => _NfcReaderPageState(mrzResult);
}

class _NfcReaderPageState extends State<NfcReaderPage> {
  final MRZResult mrzResult;
  _NfcReaderPageState(this.mrzResult);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      body: Body(
        mrzResult: mrzResult,
      ),
    );
  }
}
