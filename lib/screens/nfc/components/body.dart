import 'dart:async';
import 'dart:convert';

import 'package:e_passport/LocalKeys.dart';
import 'package:e_passport/general.dart';
import 'package:e_passport/models/nfc_card/nfc_card.dart';
import 'package:e_passport/models/nfc_card/user.dart';
import 'package:e_passport/screens/nfc/components/read_result.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:mrz_parser/mrz_parser.dart';
import 'package:easy_localization/easy_localization.dart';

class Body extends StatefulWidget {
  final MRZResult mrzResult;

  Body({Key key, this.mrzResult}) : super(key: key);

  @override
  _BodyState createState() => _BodyState(mrzResult);
}

class _BodyState extends State<Body> {
  static const platform = MethodChannel('nfc_reader_activity');
  final MRZResult mrzResult;
  String result;
  Future<User> futureUser;
  NfcCard card;

  _BodyState(this.mrzResult);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _readIdCard(
          mrzResult.documentNumber, mrzResult.birthDate, mrzResult.expiryDate),
      builder: (context, AsyncSnapshot<NfcCard> snapshot) {
        if (snapshot.hasData) {
          return ResultForm(card: this.card);
        } else if (snapshot.hasError) {
          return Container(
            child: Center(
              child: Text(LocalKeys.errorAlertTitle).tr(),
            ),
          );
        } else {
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
  }

  Future<NfcCard> _readIdCard(passportNumber, birthDate, expireDate) async {
    try {
      print("f: before");
      result = await platform.invokeMethod('nfc_reader_activity', {
        'passportNumber': passportNumber,
        'dateOfBirth': formatDate(birthDate),
        'dateOfExpiry': formatDate(expireDate)
      });
      // result = await platform.invokeMethod('aa_challenge_method', {
      //   // 'challenge': "00B0900010",
      //   'challenge': "Ily7nM5xMf0=",
      // });
      print("f: after");
      print(result);
      Map cardMap = jsonDecode(result);
      card = NfcCard.fromJson(cardMap);
      return card;
    } on Exception catch (e) {
      flushBar(
          context, LocalKeys.cardReadFailedAlertTitle.tr(), e.toString(), true);
      return null;
    }
  }

  String formatDate(DateTime dateTime) {
    return DateFormat('yyyy-MM-dd').format(dateTime);
  }
}
