import 'dart:convert';

import 'package:e_passport/LocalKeys.dart';
import 'package:e_passport/models/nfc_card/nfc_card.dart';
import 'package:e_passport/screens/auth/auth_page.dart';
import 'package:e_passport/utils/http_module.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

import '../../../constants.dart';
import '../../../general.dart';

class ResultForm extends StatefulWidget {
  NfcCard card;
  ResultForm({Key key, this.card}) : super(key: key);

  @override
  _ResultFormState createState() => _ResultFormState(this.card);
}

class _ResultFormState extends State<ResultForm> {
  NfcCard card;

  HttpModule httpModule;

  _ResultFormState(this.card);

  @override
  void initState() {
    httpModule = HttpModule();
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => {
          if (card != null && card.user != null)
            {
              flushBar(context, LocalKeys.cardReadSuccessAlertTitle.tr(),
                  LocalKeys.cardReadSuccessAlertBody.tr(), false)
            }
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            Expanded(
              child: FittedBox(
                fit: BoxFit.fill,
                child: Image.memory(
                  base64Decode(card.user.imageBase64),
                ),
              ),
            ),
            TextFormField(
              cursorColor: Theme.of(context).cursorColor,
              initialValue: card.user.firstName,
              readOnly: true,
              decoration: InputDecoration(
                labelText: LocalKeys.cardFormFirstName.tr(),
                labelStyle: TextStyle(
                  color: kTextColor,
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: kPrimaryColor),
                ),
              ),
            ),
            TextFormField(
              cursorColor: Theme.of(context).cursorColor,
              initialValue: card.user.lastName,
              readOnly: true,
              decoration: InputDecoration(
                labelText: LocalKeys.cardFormLastName.tr(),
                labelStyle: TextStyle(
                  color: kTextColor,
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: kPrimaryColor),
                ),
              ),
            ),
            TextFormField(
              cursorColor: Theme.of(context).cursorColor,
              initialValue: card.user.nationality,
              readOnly: true,
              decoration: InputDecoration(
                labelText: LocalKeys.cardFormNationality.tr(),
                labelStyle: TextStyle(
                  color: kTextColor,
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: kPrimaryColor),
                ),
              ),
            ),
            TextFormField(
              cursorColor: Theme.of(context).cursorColor,
              initialValue: card.user.documentNumber,
              readOnly: true,
              decoration: InputDecoration(
                labelText: LocalKeys.cardFormDocumentNumber.tr(),
                labelStyle: TextStyle(
                  color: kTextColor,
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: kPrimaryColor),
                ),
              ),
            ),
            TextFormField(
              cursorColor: Theme.of(context).cursorColor,
              initialValue: card.user.personalNumber,
              readOnly: true,
              decoration: InputDecoration(
                labelText: LocalKeys.cardFormPersonalNumber.tr(),
                labelStyle: TextStyle(
                  color: kTextColor,
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: kPrimaryColor),
                ),
              ),
            ),
            ElevatedButton(
              child: Text(LocalKeys.toRegistrationPageButton).tr(),
              onPressed: () async {
                await Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AuthScreen(card: card)));
              },
            )
          ],
        ),
      ),
    );
  }
}
