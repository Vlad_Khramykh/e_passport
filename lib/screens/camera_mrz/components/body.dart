import 'package:e_passport/LocalKeys.dart';
import 'package:e_passport/screens/nfc/nfc_reader_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mrz_scanner/flutter_mrz_scanner.dart';
import 'package:flutter_nfc_kit/flutter_nfc_kit.dart';
import '../../../general.dart';
import 'package:easy_localization/easy_localization.dart';
import 'camera_overlay.dart';

class Body extends StatefulWidget {
  final bool isEPassport;

  Body({Key key, this.isEPassport}) : super(key: key);

  @override
  _CameraPageState createState() =>
      _CameraPageState(isEPassport: this.isEPassport);
}

class _CameraPageState extends State<Body> {
  bool isParsed = false;
  bool isFlashLightEnabled = false;
  bool isFrontCameraEnabled = false;
  bool isEPassport;
  NFCAvailability nfcAvailability;
  MRZController controller;
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

  _CameraPageState({bool isEPassport}) {
    this.isEPassport = isEPassport;
  }

  @override
  void initState() {
    super.initState();
    checkNfcAvailability();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldState,
        appBar: AppBar(
          title: Text(LocalKeys.mrzScannerPageTitle).tr(),
        ),
        body: Column(
          children: <Widget>[
            Flexible(
              child: CameraOverlay(
                isEPassport: this.isEPassport,
                child: MRZScanner(
                    onControllerCreated: (controller) => {
                          onControllerCreated(controller),
                          controller?.startPreview()
                        }),
              ),
            ),
            // Container(
            //   height: size.height * 0.15,
            //   child: Row(
            //     children: [
            //       FloatingActionButton(
            //           child: const Icon(Icons.highlight),
            //           onPressed: () {
            //             isFlashLightEnabled
            //                 ? controller.flashlightOff()
            //                 : controller.flashlightOn();
            //             isFlashLightEnabled = !isFlashLightEnabled;
            //           }),
            //     ],
            //   ),
            // ),
          ],
        ));
  }

  void onControllerCreated(MRZController controller) {
    this.controller = controller;

    controller.onParsed = (result) async {
      if (isParsed) {
        return;
      } else {
        nfcAvailability = await FlutterNfcKit.nfcAvailability;
        if (nfcAvailability == NFCAvailability.disabled) {
          setState(() => isParsed = true);
          alertWidget(
                  LocalKeys.nfcAlertTitle.tr(),
                  LocalKeys.enableNfcForNextStateText.tr(),
                  _scaffoldState.currentContext)
              .then((value) {
            setState(() => isParsed = false);
          });
        } else if (nfcAvailability == NFCAvailability.not_supported) {
          setState(() => isParsed = true);
          alertWidget(LocalKeys.nfcAlertTitle.tr(),
                  LocalKeys.nfcNotSupported.tr(), _scaffoldState.currentContext)
              .then((value) => setState(() => isParsed = false));
        } else if (nfcAvailability == NFCAvailability.available) {
          setState(() => isParsed = true);
          await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => NfcReaderPage(
                        mrzResult: result,
                      )));
          controller.stopPreview();
          return;
        }
      }
    };
    controller.onError = (error) => flushBar(
        context, LocalKeys.errorAlertTitle.tr(), error.toString(), true);
  }

  checkNfcAvailability() {
    FlutterNfcKit.nfcAvailability.then((value) => {
          setState(() {
            nfcAvailability = value;
          })
        });
  }
}
