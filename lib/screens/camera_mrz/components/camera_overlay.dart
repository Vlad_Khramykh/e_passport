import 'package:e_passport/constants.dart';
import 'package:flutter/material.dart';

class CameraOverlay extends StatelessWidget {
  bool isEPassport;

  CameraOverlay({Key key, bool isEPassport, this.child}) : super(key: key) {
    this.isEPassport = isEPassport;
  }

  static const _documentFrameRatio =
      1.42; // Passport's size (ISO/IEC 7810 ID-3) is 125mm × 88mm
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (_, c) {
        final overlayRect =
            _calculateOverlaySize(Size(c.maxWidth, c.maxHeight));
        return Stack(
          children: [
            child,
            ClipPath(
              clipper: _DocumentClipper(rect: overlayRect),
              child: Container(
                foregroundDecoration: const BoxDecoration(
                  color: Color.fromRGBO(0, 0, 0, 0.5),
                ),
              ),
            ),
            _WhiteOverlay(rect: overlayRect, isEPassport: isEPassport),
          ],
        );
      },
    );
  }

  RRect _calculateOverlaySize(Size size) {
    double width, height;
    if (size.height > size.width) {
      width = size.width * 0.9;
      height = width / _documentFrameRatio;
    } else {
      height = size.height * 0.75;
      width = height * _documentFrameRatio;
    }
    final topOffset = (size.height - height) / 2;
    final leftOffset = (size.width - width) / 2;

    final rect = RRect.fromLTRBR(leftOffset, topOffset, leftOffset + width,
        topOffset + height, const Radius.circular(8));
    return rect;
  }
}

class _DocumentClipper extends CustomClipper<Path> {
  _DocumentClipper({this.rect});

  final RRect rect;

  @override
  Path getClip(Size size) => Path()
    ..addRRect(rect)
    ..addRect(Rect.fromLTWH(0.0, 0.0, size.width, size.height))
    ..fillType = PathFillType.evenOdd;

  @override
  bool shouldReclip(_DocumentClipper oldClipper) => false;
}

class _WhiteOverlay extends StatelessWidget {
  bool isEPassport;
  _WhiteOverlay({Key key, bool isEPassport, this.rect}) : super(key: key) {
    this.isEPassport = isEPassport;
  }
  final RRect rect;

  @override
  Widget build(BuildContext context) {
    return this.isEPassport ? ePassportMrzTemplate() : passportMrzTemplate();
  }

  Positioned ePassportMrzTemplate() {
    return Positioned(
      left: rect.left,
      top: rect.top,
      child: Container(
          width: rect.width,
          height: rect.height,
          decoration: BoxDecoration(
            border: Border.all(width: 2.0, color: const Color(0xFFFFFFFF)),
            borderRadius: BorderRadius.all(rect.tlRadius),
          ),
          child: Padding(
              padding: EdgeInsets.only(
                  top: rect.width * 0.5, right: 15, bottom: 10, left: 15),
              child: FittedBox(
                alignment: Alignment.bottomCenter,
                fit: BoxFit.fill,
                child: new Text(
                  ePassportMRZTemplate,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white70,
                    fontFamily: 'OverpassMono-SemiBold',
                  ),
                ),
              ))),
    );
  }

  Positioned passportMrzTemplate() {
    return Positioned(
      left: rect.left,
      top: rect.top,
      child: Container(
          width: rect.width,
          height: rect.height,
          decoration: BoxDecoration(
            border: Border.all(width: 2.0, color: const Color(0xFFFFFFFF)),
            borderRadius: BorderRadius.all(rect.tlRadius),
          ),
          child: Padding(
              padding: EdgeInsets.only(
                  top: rect.width * 0.5, right: 15, bottom: 10, left: 15),
              child: FittedBox(
                alignment: Alignment.bottomCenter,
                fit: BoxFit.fitWidth,
                child: new Text(
                  passportMRZTemplate,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white70,
                    fontFamily: 'OverpassMono-SemiBold',
                  ),
                ),
              ))),
    );
  }
}
