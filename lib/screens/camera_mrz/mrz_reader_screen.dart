import 'package:e_passport/LocalKeys.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import '../../constants.dart';
import 'components/body.dart';
import 'package:easy_localization/easy_localization.dart';

class MRZReaderScreen extends StatelessWidget {
  bool _isEPassport;

  MRZReaderScreen({bool isEPassport}) {
    this._isEPassport = isEPassport;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: FutureBuilder<bool>(
        future: requestPermissions(),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data) {
            return Body(isEPassport: _isEPassport);
          } else if (snapshot.hasData && !snapshot.data) {
            Navigator.of(context).pop();
          } else {
            return Scaffold(
              body: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircularProgressIndicator(),
                    Text(LocalKeys.awaitingForPermissions).tr(),
                  ],
                ),
              ),
            );
          }
        },
      ),
      theme: ThemeData(
          scaffoldBackgroundColor: kBackgroundColor,
          primaryColor: kPrimaryColor,
          textTheme: Theme.of(context).textTheme.apply(bodyColor: kTextColor),
          visualDensity: VisualDensity.adaptivePlatformDensity),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      elevation: 0,
      title: Text(LocalKeys.title).tr(),
    );
  }

  Future<bool> requestPermissions() async {
    final status = await Permission.camera.request();
    if (status.isGranted) {
      return true;
    } else {
      return false;
    }
  }
}
