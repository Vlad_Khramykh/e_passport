import 'package:e_passport/LocalKeys.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_nfc_kit/flutter_nfc_kit.dart';
import '../../../constants.dart';
import '../../../utils/adpu_helper.dart';
import 'package:easy_localization/easy_localization.dart';

class Body extends StatelessWidget {
  NFCAvailability available;
  String message;
  AdpuHelper _apduHelper = new AdpuHelper();

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Column(
      children: [
        Container(
            height: size.height * 0.2,
            child: Stack(children: <Widget>[
              Container(
                child: Row(
                  children: [
                    Expanded(
                      flex: 8,
                      child: Text('Khramykh Vladislav'),
                    ),
                    Expanded(
                      flex: 2,
                      child: CircleAvatar(
                        radius: 30,
                      ),
                    ),
                  ],
                ),
                height: size.height * 0.2 - 27,
                decoration: const BoxDecoration(
                  color: kPrimaryColor,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(36),
                      bottomRight: Radius.circular(36)),
                ),
              ),
            ])),
        Column(
          children: <Widget>[
            Center(
              child: ElevatedButton(
                child: Text(LocalKeys.scanIdCard).tr(),
                onPressed: () {
                  Navigator.pushNamed(context, '/scan_id_card_mrz');
                },
              ),
            ),
            Center(
              child: ElevatedButton(
                child: Text(LocalKeys.scanPassport).tr(),
                onPressed: () {
                  Navigator.pushNamed(context, '/scan_passport_mrz');
                },
              ),
            ),
            // Center(
            //   child: ElevatedButton(
            //     child: const Text('APDU helper test'),
            //     onPressed: () async {
            //       if ((await isNfcAvailable())) {
            //         await _apduHelper.test();
            //       } else {
            //         alertWidget("APDU", message, context);
            //       }
            //     },
            //   ),
            // ),
          ],
        )
      ],
    );
  }

  Future<bool> isNfcAvailable() async {
    available = await FlutterNfcKit.nfcAvailability;
    message = null;
    if (available == NFCAvailability.disabled) {
      message = LocalKeys.enableNfc.tr();
      return false;
    } else if (available == NFCAvailability.not_supported) {
      message = LocalKeys.nfcNotSupported.tr();
      return false;
    }
    return true;
  }
}
