import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'components/body.dart';
import 'package:easy_localization/easy_localization.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: Body(),
    );
  }

  AppBar buildAppBar(context) {
    return AppBar(
      elevation: 0,
      title: Text("title").tr(),
    );
  }
}
