import 'package:e_passport/LocalKeys.dart';
import 'package:e_passport/models/nfc_card/nfc_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'components/body.dart';
import 'package:easy_localization/easy_localization.dart';

class AuthScreen extends StatelessWidget {
  NfcCard card;

  AuthScreen({Key key, this.card}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      body: Body(card: card),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      elevation: 0,
      title: Text(LocalKeys.title).tr(),
    );
  }
}
