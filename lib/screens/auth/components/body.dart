import 'package:e_passport/LocalKeys.dart';
import 'package:e_passport/models/nfc_card/nfc_card.dart';
import 'package:e_passport/models/rest/challenge_types.dart';
import 'package:e_passport/models/rest/responses/card_challenge.dart';
import 'package:e_passport/models/rest/responses/registration_response.dart';
import 'package:e_passport/models/rest/responses/session.dart';
import 'package:e_passport/models/rest/responses/success_challenge.dart';
import 'package:e_passport/utils/http_module.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_nfc_kit/flutter_nfc_kit.dart';
import 'package:easy_localization/easy_localization.dart';

class Body extends StatefulWidget {
  NfcCard card;

  Body({Key key, this.card}) : super(key: key);

  @override
  _BodyState createState() => _BodyState(card);
}

class _BodyState extends State<Body> {
  NFCAvailability available;
  String message;
  NfcCard card;

  List<Widget> registrationRow;
  List<Widget> openSessionRow;
  List<Widget> anticloneCheckRow;

  HttpModule httpModule;

  bool isResgistrationDone = false;
  bool isSessionOpened = true;
  bool anticloneCheckIsPassed = true;

  _BodyState(this.card);

  @override
  void initState() {
    registrationRow = <Widget>[];
    openSessionRow = <Widget>[];
    anticloneCheckRow = <Widget>[];

    httpModule = HttpModule();
    super.initState();
    setState(() {
      registrationRow.add(Text(LocalKeys.registrationRowText).tr());
      openSessionRow.add(Text(LocalKeys.authenticationRowText).tr());
      anticloneCheckRow.add(Text(LocalKeys.anticloneCheckingRowText).tr());
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Column(children: [
      SizedBox(
        height: 50,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: registrationRow,
        ),
      ),
      SizedBox(
        height: 50,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: openSessionRow,
        ),
      ),
      SizedBox(
        height: 50,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: anticloneCheckRow,
        ),
      ),
      RaisedButton(
          child: Text(LocalKeys.authenticateButtonText).tr(),
          onPressed: () async {
            authentiction();
          })
    ]);
  }

  Future<bool> isNfcAvailable() async {
    available = await FlutterNfcKit.nfcAvailability;
    message = null;
    if (available == NFCAvailability.disabled) {
      message = LocalKeys.enableNfc.tr();
      return false;
    } else if (available == NFCAvailability.not_supported) {
      message = LocalKeys.nfcNotSupported.tr();
      return false;
    }
    return true;
  }

  authentiction() async {
    isResgistrationDone = false;
    setState(() {
      registrationRow.add(CircularProgressIndicator());
    });

    RegistrationResponse keys = await httpModule.registration(card);
    print(keys);
    if (keys == null) {
      setState(() {
        isResgistrationDone = false;
        registrationRow.last = Text(
          LocalKeys.authenticationFailedText.tr(),
          style: TextStyle(color: Colors.redAccent),
        );
      });
    } else {
      setState(() {
        isResgistrationDone = true;
        registrationRow.last = Text(
          LocalKeys.authenticationPassedText.tr(),
          style: TextStyle(color: Colors.greenAccent),
        );
        openSessionRow.add(CircularProgressIndicator());
      });
      Session session = await httpModule.authenticate(keys);
      print(session);
      if (session == null) {
        setState(() {
          isSessionOpened = false;
          openSessionRow.last = Text(
            LocalKeys.authenticationFailedText.tr(),
            style: TextStyle(color: Colors.redAccent),
          );
        });
      } else {
        setState(() {
          isSessionOpened = true;
          openSessionRow.last = Text(
            LocalKeys.authenticationPassedText.tr(),
            style: TextStyle(color: Colors.greenAccent),
          );
          anticloneCheckRow.add(CircularProgressIndicator());
        });
        CheckCardChallenge checkCardChallenge =
            await httpModule.checkCardChallenge(session, keys);
        if (checkCardChallenge == null) {
          setState(() {
            anticloneCheckIsPassed = false;
            anticloneCheckRow.last = Text(
              LocalKeys.authenticationFailedText.tr(),
              style: TextStyle(color: Colors.redAccent),
            );
          });
        } else {
          if (checkCardChallenge.protocol == "activeAuth") {
            SuccessChallenge successChallenge =
                await httpModule.sendAnticloneCardChallengeResult(
                    session, keys, checkCardChallenge);
            if (successChallenge != null) {
              if (successChallenge.result == "200") {
                CheckCardChallenge checkCardChallenge2 =
                    await httpModule.checkCardChallenge(session, keys);
                if (checkCardChallenge2.protocol == "biometry") {
                  print(checkCardChallenge2.toMap());
                  setState(() {
                    anticloneCheckIsPassed = true;
                    anticloneCheckRow.last = Text(
                      LocalKeys.authenticationPassedText.tr(),
                      style: TextStyle(color: Colors.greenAccent),
                    );
                  });
                } else {
                  print("F");
                }
              } else {
                print("F1");
              }
            } else {
              print("F2");
            }
          } else {
            print("F3");
          }
        }
      }
    }
  }
}
