class CardBase64 {
  String DG1;
  String DG2;
  String DG11;
  String DG12;
  String DG14;
  String DG15;
  String EF_COM;
  String EF_SOD;

  CardBase64({
    this.DG1,
    this.DG2,
    this.DG11,
    this.DG12,
    this.DG14,
    this.DG15,
    this.EF_COM,
    this.EF_SOD,
  });

  CardBase64.fromJson(Map<String, dynamic> json)
      : EF_COM = json['EF_COM'],
        EF_SOD = json['EF_SOD'],
        DG1 = json['DG1'],
        DG2 = json['DG2'],
        DG11 = json['DG11'],
        DG12 = json['DG12'],
        DG14 = json['DG14'],
        DG15 = json['DG15'];

  Map<String, dynamic> toMap() {
    return {
      'EF_COM': EF_COM,
      'EF_SOD': EF_SOD,
      'DG1': DG1,
      'DG2': DG2,
      'DG11': DG11,
      'DG12': DG12,
      'DG14': DG14,
      'DG15': DG15,
    };
  }
}
