import 'package:e_passport/models/nfc_card/card_base_64.dart';

import 'user.dart';

class NfcCard {
  CardBase64 cardBase64;
  User user;

  NfcCard(this.cardBase64, this.user);

  factory NfcCard.fromJson(Map<String, dynamic> json) {
    return NfcCard(
        CardBase64.fromJson(json['cardBase64']), User.fromJson(json['user']));
  }
}
