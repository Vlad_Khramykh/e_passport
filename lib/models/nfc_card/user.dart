class User {
  String firstName;
  String lastName;
  String gender;
  String issuingState;
  String nationality;
  String documentNumber;
  String personalNumber;
  String dateOfExpiry;
  String dateOfBirth;
  bool passiveAuthSuccess;
  bool chipAuthSucceeded;
  String imageBase64;

  User(
      {this.firstName,
      this.lastName,
      this.gender,
      this.issuingState,
      this.nationality,
      this.documentNumber,
      this.personalNumber,
      this.dateOfExpiry,
      this.dateOfBirth,
      this.passiveAuthSuccess,
      this.chipAuthSucceeded,
      this.imageBase64});

  User.fromJson(Map<String, dynamic> json)
      : firstName = json['firstName'],
        lastName = json['lastName'],
        gender = json['gender'],
        issuingState = json['issuingState'],
        nationality = json['nationality'],
        documentNumber = json['documentNumber'],
        personalNumber = json['personalNumber'],
        dateOfExpiry = json['dateOfExpiry'],
        dateOfBirth = json['dateOfBirth'],
        passiveAuthSuccess = json['passiveAuthSuccess'],
        chipAuthSucceeded = json['chipAuthSucceeded'],
        imageBase64 = json['imageBase64'];

  Map<String, dynamic> toMap() {
    return {
      'firstName': firstName,
      'lastName': lastName,
      'gender': gender,
      'issuingState': issuingState,
      'nationality': nationality,
      'documentNumber': documentNumber,
      'personalNumber': personalNumber,
      'dateOfExpiry': dateOfExpiry,
      'dateOfBirth': dateOfBirth,
      'passiveAuthSuccess': passiveAuthSuccess,
      'chipAuthSucceeded': chipAuthSucceeded,
      'imageBase64': imageBase64,
    };
  }
}
