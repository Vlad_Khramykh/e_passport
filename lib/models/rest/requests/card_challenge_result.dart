import 'package:e_passport/models/rest/responses/card_challenge.dart';

class CardChallengeResult {
  CheckCardChallenge cardChallenge;
  String response;

  CardChallengeResult();

  CardChallengeResult.fromJson(Map<String, dynamic> json)
      : cardChallenge = CheckCardChallenge.fromJson(json['check']),
        response = json['response'];

  Map<String, dynamic> toMap() {
    return {
      'check': cardChallenge.toMap(),
      'response': response,
    };
  }
}
