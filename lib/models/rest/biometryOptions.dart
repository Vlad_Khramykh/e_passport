class BiometryOptions {
  double faceMatching;
  double dynamicVoice;
  double staticVoice;
  double fused;
  bool isAlive;
}
