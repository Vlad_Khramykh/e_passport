class CheckResult {
  bool polling;
  String result;

  CheckResult.fromJson(Map<String, dynamic> json)
      : polling = json['polling'],
        result = json['result'];
}
