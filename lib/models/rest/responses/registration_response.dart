class RegistrationResponse {
  String apiKey;
  String mrtdID;

  RegistrationResponse.fromJson(Map<String, dynamic> json)
      : apiKey = json['apiKey'],
        mrtdID = json['mrtdID'];
}
