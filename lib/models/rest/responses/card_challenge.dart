class CheckCardChallenge {
  String protocol;
  String checkId;
  String respUrl;
  String videoURL;
  String pollingURL;
  String pollingInterval;
  String challenge;

  CheckCardChallenge.fromJson(Map<String, dynamic> json)
      : protocol = json['protocol'],
        checkId = json['check_id'],
        respUrl = json['respURL'],
        challenge = json['challenge'],
        videoURL = json['videoURL'],
        pollingURL = json['pollingURL'],
        pollingInterval = json['pollingInterval'];

  Map<String, dynamic> toMap() {
    return {
      'protocol': protocol,
      'check_id': checkId,
      'respURL': respUrl,
      'challenge': challenge,
      'videoURL': videoURL,
      'pollingURL': pollingURL,
      'pollingInterval': pollingInterval
    };
  }
}
