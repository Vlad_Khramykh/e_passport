class SuccessChallenge {
  bool polling;
  String result;

  SuccessChallenge.fromJson(Map<String, dynamic> json)
      : polling = json['polling'],
        result = json['result'];
}
