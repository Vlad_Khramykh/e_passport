import 'package:easy_localization/easy_localization.dart';
import 'package:easy_localization_loader/easy_localization_loader.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'constants.dart';
import 'screens/camera_mrz/mrz_reader_screen.dart';
import 'screens/home/home_screen.dart';
import 'screens/nfc/nfc_reader_page.dart';

void main() {
  runApp(
    EasyLocalization(
        supportedLocales: [Locale('en', 'US'), Locale('ru', 'RU')],
        path: 'assets/translations',
        fallbackLocale: Locale('en', 'US'),
        assetLoader: YamlAssetLoader(),
        child: MyApp()),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      home: HomeScreen(),
      debugShowCheckedModeBanner: false,
      routes: {
        '/scan_id_card_mrz': (context) => MRZReaderScreen(isEPassport: true),
        '/read_nfc_id_card': (context) => NfcReaderPage(),
        '/scan_passport_mrz': (context) => MRZReaderScreen(isEPassport: false),
      },
      theme: ThemeData(
          scaffoldBackgroundColor: kBackgroundColor,
          primaryColor: kPrimaryColor,
          textTheme: Theme.of(context).textTheme.apply(bodyColor: kTextColor),
          visualDensity: VisualDensity.adaptivePlatformDensity),
    );
  }
}
