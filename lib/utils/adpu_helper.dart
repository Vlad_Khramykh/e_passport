import 'package:flutter_nfc_kit/flutter_nfc_kit.dart';

// NFCTagType.iso7816 - E-Passport

class AdpuHelper {
  var _result;

  Future<void> test() async {
    try {
      NFCTag tag = await FlutterNfcKit.poll();
      await FlutterNfcKit.setIosAlertMessage("Sending APDU");
      if (tag.type == NFCTagType.iso7816) {
        var result = await FlutterNfcKit.transceive(
          "00B0900010",
        );
        print("result: " + result);
      }
      print(_result);
    } catch (e) {
      _result = 'error: $e';
    }
  }
}
