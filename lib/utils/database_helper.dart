import 'dart:async';

import 'package:e_passport/models/nfc_card/user.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:flutter/widgets.dart';

class DatabaseHelper {
  Future<Database> database;

  DatabaseHelper() {
    WidgetsFlutterBinding.ensureInitialized();
    getDatabasesPath().then((dbPath) => {
          this.database = openDatabase(
            join(dbPath, 'cards_database.db'),
            onCreate: (db, version) {
              return db.execute(
                "CREATE TABLE cards(id INTEGER PRIMARY KEY, name TEXT, age INTEGER)",
              );
            },
            version: 1,
          )
        });
  }

  Future<void> insertUser(User user) async {
    final Database db = await database;

    await db.insert(
      'cards',
      user.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<User>> cards() async {
    final Database db = await database;
    final List<Map<String, dynamic>> maps = await db.query('dogs');

    return List.generate(maps.length, (i) {
      return User(
        firstName: maps[i]['firstName'],
        lastName: maps[i]['lastName'],
        gender: maps[i]['gender'],
        issuingState: maps[i]['issuingState'],
        nationality: maps[i]['nationality'],
        documentNumber: maps[i]['documentNumber'],
        personalNumber: maps[i]['personalNumber'],
        dateOfExpiry: maps[i]['dateOfExpiry'],
        dateOfBirth: maps[i]['dateOfBirth'],
        passiveAuthSuccess: maps[i]['passiveAuthSuccess'],
        chipAuthSucceeded: maps[i]['chipAuthSucceeded'],
        imageBase64: maps[i]['imageBase64'],
      );
    });
  }
}
