import 'dart:io';

import 'package:e_passport/constants.dart';
import 'package:e_passport/models/rest/requests/card_challenge_result.dart';
import 'package:e_passport/models/rest/responses/card_challenge.dart';
import 'package:e_passport/models/rest/responses/registration_response.dart';
import 'package:e_passport/models/rest/responses/session.dart';
import 'package:e_passport/models/rest/responses/success_challenge.dart';
import '../models/nfc_card/nfc_card.dart';
import 'dart:convert' as convert;

class HttpModule {
  HttpClient client;

  HttpModule() {
    client = new HttpClient();
    client.badCertificateCallback = (_, __, ___) => true;
  }

  Future<RegistrationResponse> registration(NfcCard card) async {
    final request = await client
        .postUrl(Uri.parse(MAIN_URL + ":" + PORT + REGISTRTION_URL));
    request.headers.set("Content-Type", "application/json");
    var json = convert.jsonEncode(card.cardBase64.toMap());
    request.write(convert.jsonEncode(card.cardBase64.toMap()));
    final response = await request.close();
    if (response.statusCode == 201) {
      var jsonBody = await response.transform(convert.utf8.decoder).join();
      return RegistrationResponse.fromJson(convert.jsonDecode(jsonBody));
    } else {
      print(response.statusCode);
      return null;
    }
  }

  Future<Session> authenticate(RegistrationResponse keys) async {
    final request =
        await client.getUrl(Uri.parse(MAIN_URL + ":" + PORT + OPENSESSION_URL));
    request.headers
        .add("Content-Type", "application/json", preserveHeaderCase: true);
    request.headers.add("X-API-Key", keys.apiKey, preserveHeaderCase: true);
    request.headers.add("X-MRTD-ID", keys.mrtdID, preserveHeaderCase: true);
    final response = await request.close();
    if (response.statusCode == 200) {
      var jsonBody = await response.transform(convert.utf8.decoder).join();
      return Session.fromJson(convert.jsonDecode(jsonBody));
    } else {
      return null;
    }
  }

  Future<CheckCardChallenge> checkCardChallenge(
      Session session, RegistrationResponse keys) async {
    final request =
        await client.getUrl(Uri.parse(MAIN_URL + ":" + PORT + CHEKING_URL));
    request.headers.set("Content-Type", "application/json");
    request.headers.set("X-API-Key", keys.apiKey, preserveHeaderCase: true);
    request.headers.set("X-MRTD-ID", keys.mrtdID, preserveHeaderCase: true);
    request.headers
        .set("X-Session-Id", session.sessionId, preserveHeaderCase: true);
    final response = await request.close();
    if (response.statusCode == 200) {
      var jsonBody = await response.transform(convert.utf8.decoder).join();
      return CheckCardChallenge.fromJson(convert.jsonDecode(jsonBody));
    } else {
      return null;
    }
  }

  Future<SuccessChallenge> sendAnticloneCardChallengeResult(Session session,
      RegistrationResponse keys, CheckCardChallenge checkCardChallenge) async {
    final request = await client.postUrl(Uri.parse(checkCardChallenge.respUrl));
    request.headers.set("Content-Type", "application/json");
    request.headers.set("X-API-Key", keys.apiKey, preserveHeaderCase: true);
    request.headers.set("X-MRTD-ID", keys.mrtdID, preserveHeaderCase: true);
    request.headers
        .set("X-Session-Id", session.sessionId, preserveHeaderCase: true);
    CardChallengeResult result = CardChallengeResult();
    result.cardChallenge = checkCardChallenge;
    result.response = checkCardChallenge.challenge;
    var json = convert.jsonEncode(result.toMap());
    request.write(json);
    final response = await request.close();
    if (response.statusCode == 200) {
      var jsonBody = await response.transform(convert.utf8.decoder).join();
      return SuccessChallenge.fromJson(convert.jsonDecode(jsonBody));
    } else {
      return null;
    }
  }
}
