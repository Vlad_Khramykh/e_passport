package com.wedextim.e_passport;

public final class Card {
    public CardBase64 cardBase64;
    public User user;

    public Card(CardBase64 cardBase64, User user) {
        this.cardBase64 = cardBase64;
        this.user = user;
    }
}
