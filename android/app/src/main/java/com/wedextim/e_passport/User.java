package com.wedextim.e_passport;

public class User {
    public String firstName;
    public String lastName;
    public String gender;
    public String issuingState;
    public String nationality;
    public String documentNumber;
    public String personalNumber;
    public String dateOfExpiry;
    public String dateOfBirth;
    public boolean passiveAuthSuccess;
    public boolean chipAuthSucceeded;
    public String imageBase64;
}