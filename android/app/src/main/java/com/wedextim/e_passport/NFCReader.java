package com.wedextim.e_passport;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;

import net.sf.scuba.smartcards.CardFileInputStream;
import net.sf.scuba.smartcards.CardService;
import net.sf.scuba.smartcards.CardServiceException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.x509.Certificate;
import org.jmrtd.BACKey;
import org.jmrtd.BACKeySpec;
import org.jmrtd.PassportService;
import org.jmrtd.lds.CardAccessFile;
import org.jmrtd.lds.ChipAuthenticationPublicKeyInfo;
import org.jmrtd.lds.PACEInfo;
import org.jmrtd.lds.SODFile;
import org.jmrtd.lds.SecurityInfo;
import org.jmrtd.lds.icao.COMFile;
import org.jmrtd.lds.icao.DG11File;
import org.jmrtd.lds.icao.DG12File;
import org.jmrtd.lds.icao.DG14File;
import org.jmrtd.lds.icao.DG15File;
import org.jmrtd.lds.icao.DG1File;
import org.jmrtd.lds.icao.DG2File;
import org.jmrtd.lds.icao.MRZInfo;
import org.jmrtd.lds.iso19794.FaceImageInfo;
import org.jmrtd.lds.iso19794.FaceInfo;
import org.jmrtd.protocol.AAResult;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.CertPath;
import java.security.cert.CertPathValidator;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXParameters;
import java.security.cert.X509Certificate;
import java.security.spec.MGF1ParameterSpec;
import java.security.spec.PSSParameterSpec;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.wedextim.e_passport.MainMethodResolver.ISO7816_AA_CARD_CHALLENGE;
import static com.wedextim.e_passport.MainMethodResolver.ISO7816_READ_CARD_RES_ERROR_CHIP_AUTH;
import static com.wedextim.e_passport.MainMethodResolver.ISO7816_READ_CARD_RES_ERROR_DATA_NOT_FOUND;
import static com.wedextim.e_passport.MainMethodResolver.ISO7816_READ_CARD_RES_ERROR_MRZ_VALIDATE;
import static com.wedextim.e_passport.MainMethodResolver.ISO7816_READ_CARD_RES_OK;
import static org.jmrtd.PassportService.DEFAULT_MAX_BLOCKSIZE;
import static org.jmrtd.PassportService.NORMAL_MAX_TRANCEIVE_LENGTH;

public class NFCReader extends Activity {

    private static final String TAG = NFCReader.class.getSimpleName();

    Intent intent;
    Activity activity;
    AssetManager assetManager;

    private static String PASSPORT_NUMBER = "AA9912868";
    private static String EXPIRATION_DATE = "2030-10-02";
    private static String BIRTH_DATE = "1970-12-02";

    private View mainLayout;
    private View loadingLayout;

    public NFCReader() {
        this.intent = getIntent();
        this.activity = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assetManager = getApplicationContext().getAssets();
        System.out.println("NFCReader.challenge: " + getIntent().getStringExtra("challenge"));
        System.out.println("NFCReader.dateOfBirth: " + getIntent().getStringExtra("dateOfBirth"));
        System.out.println("NFCReader.dateOfExpiry: " + getIntent().getStringExtra("dateOfExpiry"));
        System.out.println("NFCReader.passportNumber: " + getIntent().getStringExtra("passportNumber"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        NfcAdapter adapter = NfcAdapter.getDefaultAdapter(this);
        if (adapter != null) {
            Intent intent = new Intent(getApplicationContext(), this.getClass());
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            String[][] filter = new String[][]{new String[]{"android.nfc.tech.IsoDep"}};
            adapter.enableForegroundDispatch(this, pendingIntent, null, filter);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        NfcAdapter adapter = NfcAdapter.getDefaultAdapter(this);
        if (adapter != null) {
            adapter.disableForegroundDispatch(this);
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(intent.getAction())) {
            Tag tag = intent.getExtras().getParcelable(NfcAdapter.EXTRA_TAG);
            if (Arrays.asList(tag.getTechList()).contains("android.nfc.tech.IsoDep")) {
                if (PASSPORT_NUMBER != null && !PASSPORT_NUMBER.isEmpty()
                        && EXPIRATION_DATE != null && !EXPIRATION_DATE.isEmpty()
                        && BIRTH_DATE != null && !BIRTH_DATE.isEmpty()) {
                    BACKeySpec bacKey = new BACKey(PASSPORT_NUMBER, convertDate(BIRTH_DATE), convertDate(EXPIRATION_DATE));

                    if (getIntent().getStringExtra("method").equals(MainMethodResolver.AA_CHALLENGE_METHOD)) {
                        System.out.println("Executing AAChallenge method");
                        new AATask(IsoDep.get(tag), bacKey, getIntent().getStringExtra("challenge")).execute();
                    } else {
                        System.out.println("Executing NFCReader method");
                        new ReadTask(IsoDep.get(tag), bacKey).execute();
                    }

                } else {
                    setResult(ISO7816_READ_CARD_RES_ERROR_MRZ_VALIDATE);
                    finish();
                }
            }
        }
    }

    private static String convertDate(String input) {
        if (input == null) {
            return null;
        }
        try {
            return new SimpleDateFormat("yyMMdd", Locale.US)
                    .format(new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(input));
        } catch (ParseException e) {
            Log.w(NFCReader.class.getSimpleName(), e);
            return null;
        }
    }

    private class AATask extends AsyncTask<Void, Void, Exception> {

        private final IsoDep isoDep;
        private final BACKeySpec bacKey;
        private PassportService service;
        private DG14File dg14File;
        private byte[] dg14Encoded = new byte[0];
        private final byte[] challenge;
        AAResult aaResult;

        private AATask(IsoDep isoDep, BACKeySpec bacKey, String challengeString) {
            this.isoDep = isoDep;
            this.bacKey = bacKey;
            challenge = Base64.decode(challengeString, Base64.DEFAULT);
            System.out.println(new String(challenge));
        }

        @Override
        protected Exception doInBackground(Void... params) {
            try {
                System.out.println("inBackground");
                CardService cardService = CardService.getInstance(isoDep);
                cardService.open();
                System.out.println("service opened");

                service = new PassportService(cardService, NORMAL_MAX_TRANCEIVE_LENGTH, DEFAULT_MAX_BLOCKSIZE, false, false);
                service.open();

                boolean paceSucceeded = false;
                try {
                    CardAccessFile cardAccessFile = new CardAccessFile(service.getInputStream(PassportService.EF_CARD_ACCESS));
                    Collection<SecurityInfo> securityInfoCollection = cardAccessFile.getSecurityInfos();
                    for (SecurityInfo securityInfo : securityInfoCollection) {
                        if (securityInfo instanceof PACEInfo) {
                            PACEInfo paceInfo = (PACEInfo) securityInfo;
                            service.doPACE(bacKey, paceInfo.getObjectIdentifier(), PACEInfo.toParameterSpec(paceInfo.getParameterId()), null);
                            paceSucceeded = true;
                        }
                    }
                } catch (Exception e) {
                    Log.w(TAG, e);
                }

                service.sendSelectApplet(paceSucceeded);

                if (paceSucceeded) {
                    service.getInputStream(PassportService.EF_COM).read();
                    aaResult = doAA(challenge);
                    System.out.println("AA_RESULT: " + aaResult);
                } else {
                    System.out.println("CATCH AA_RESULT: " + aaResult);
                }
            } catch (CardServiceException | IOException e) {
                e.printStackTrace();
                return e;
            }
            return null;
        }

        private AAResult doAA(byte[] challenge) throws IOException, CardServiceException {

            PublicKey publicKey = null;

            CardFileInputStream dg14In = service.getInputStream(PassportService.EF_DG14);
            dg14Encoded = IOUtils.toByteArray(dg14In);
            ByteArrayInputStream dg14InByte = new ByteArrayInputStream(dg14Encoded);
            dg14File = new DG14File(dg14InByte);

            Collection<SecurityInfo> dg14FileSecurityInfo = dg14File.getSecurityInfos();
            for (SecurityInfo securityInfo : dg14FileSecurityInfo) {
                if (securityInfo instanceof ChipAuthenticationPublicKeyInfo) {
                    ChipAuthenticationPublicKeyInfo publicKeyInfo = (ChipAuthenticationPublicKeyInfo) securityInfo;
                    BigInteger keyId = publicKeyInfo.getKeyId();
                    publicKey = publicKeyInfo.getSubjectPublicKey();
                    String oid = publicKeyInfo.getObjectIdentifier();
                    service.doEACCA(keyId, ChipAuthenticationPublicKeyInfo.ID_CA_ECDH_AES_CBC_CMAC_256, oid, publicKey);
                }
            }
            return service.doAA(publicKey, null, null, challenge);
        }

        @Override
        protected void onPostExecute(Exception result) {
            if (result == null) {
                System.out.println("POST_EXECUTE");
                intent.putExtra("result", aaResult.getResponse());
                setResult(ISO7816_AA_CARD_CHALLENGE, intent);
            } else {
                setResult(ISO7816_READ_CARD_RES_ERROR_DATA_NOT_FOUND);
            }
            finish();
        }


    }


    private class ReadTask extends AsyncTask<Void, Void, Exception> {

        private final IsoDep isoDep;
        private final BACKeySpec bacKey;
        private PassportService service;

        private ReadTask(IsoDep isoDep, BACKeySpec bacKey) {
            this.isoDep = isoDep;
            this.bacKey = bacKey;
        }

        private DG1File dg1File;
        private DG2File dg2File;
        private DG11File dg11File;
        private DG12File dg12File;
        private DG14File dg14File;
        private DG15File dg15File;
        private SODFile sodFile;
        private COMFile comFile;
        private String imageBase64;
        private boolean chipAuthSucceeded = false;
        private boolean passiveAuthSuccess = false;

        private byte[] dg14Encoded = new byte[0];

        private void doChipAuth(PassportService service) {
            try {
                CardFileInputStream dg14In = service.getInputStream(PassportService.EF_DG14);
                dg14Encoded = IOUtils.toByteArray(dg14In);
                ByteArrayInputStream dg14InByte = new ByteArrayInputStream(dg14Encoded);
                dg14File = new DG14File(dg14InByte);

                Collection<SecurityInfo> dg14FileSecurityInfo = dg14File.getSecurityInfos();
                for (SecurityInfo securityInfo : dg14FileSecurityInfo) {
                    if (securityInfo instanceof ChipAuthenticationPublicKeyInfo) {
                        ChipAuthenticationPublicKeyInfo publicKeyInfo = (ChipAuthenticationPublicKeyInfo) securityInfo;
                        BigInteger keyId = publicKeyInfo.getKeyId();
                        PublicKey publicKey = publicKeyInfo.getSubjectPublicKey();
                        String oid = publicKeyInfo.getObjectIdentifier();
                        service.doEACCA(keyId, ChipAuthenticationPublicKeyInfo.ID_CA_ECDH_AES_CBC_CMAC_256, oid, publicKey);
                        chipAuthSucceeded = true;
                    }
                }
            } catch (Exception e) {
                Log.w(TAG, e);
                setResult(ISO7816_READ_CARD_RES_ERROR_CHIP_AUTH);
                finish();
            }
        }

        private void doPassiveAuth() {
            try {
                MessageDigest digest = MessageDigest.getInstance(sodFile.getDigestAlgorithm());

                Map<Integer, byte[]> dataHashes = sodFile.getDataGroupHashes();

                byte[] dg14Hash = new byte[0];
                if (chipAuthSucceeded) {
                    dg14Hash = digest.digest(dg14Encoded);
                }
                byte[] dg1Hash = digest.digest(dg1File.getEncoded());
                byte[] dg2Hash = digest.digest(dg2File.getEncoded());

                if (Arrays.equals(dg1Hash, dataHashes.get(1)) && Arrays.equals(dg2Hash, dataHashes.get(2)) && (!chipAuthSucceeded || Arrays.equals(dg14Hash, dataHashes.get(14)))) {
                    // We retrieve the CSCA from the german master list
                    ASN1InputStream asn1InputStream = new ASN1InputStream(assetManager.open("masterList.pem"));
                    ASN1Primitive p;
                    KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
                    keystore.load(null, null);
                    CertificateFactory cf = CertificateFactory.getInstance("X.509");
                    while ((p = asn1InputStream.readObject()) != null) {
                        ASN1Sequence asn1 = ASN1Sequence.getInstance(p);
                        if (asn1 == null || asn1.size() == 0) {
                            throw new IllegalArgumentException("null or empty sequence passed.");
                        }
                        if (asn1.size() != 2) {
                            throw new IllegalArgumentException("Incorrect sequence size: " + asn1.size());
                        }
                        ASN1Set certSet = ASN1Set.getInstance(asn1.getObjectAt(1));

                        for (int i = 0; i < certSet.size(); i++) {
                            Certificate certificate = Certificate.getInstance(certSet.getObjectAt(i));

                            byte[] pemCertificate = certificate.getEncoded();

                            java.security.cert.Certificate javaCertificate = cf.generateCertificate(new ByteArrayInputStream(pemCertificate));
                            keystore.setCertificateEntry(String.valueOf(i), javaCertificate);
                        }
                    }
                    List<X509Certificate> docSigningCertificates = sodFile.getDocSigningCertificates();
                    for (X509Certificate docSigningCertificate : docSigningCertificates) {
                        docSigningCertificate.checkValidity();
                    }

                    // We check if the certificate is signed by a trusted CSCA
                    // TODO: verify if certificate is revoked
                    CertPath cp = cf.generateCertPath(docSigningCertificates);
                    PKIXParameters pkixParameters = new PKIXParameters(keystore);
                    pkixParameters.setRevocationEnabled(false);
                    CertPathValidator cpv = CertPathValidator.getInstance(CertPathValidator.getDefaultType());
                    cpv.validate(cp, pkixParameters);

                    String sodDigestEncryptionAlgorithm = sodFile.getDigestEncryptionAlgorithm();

                    boolean isSSA = false;
                    if (sodDigestEncryptionAlgorithm.equals("SSAwithRSA/PSS")) {
                        sodDigestEncryptionAlgorithm = "SHA256withRSA/PSS";
                        isSSA = true;
                    }

                    Signature sign = Signature.getInstance(sodDigestEncryptionAlgorithm);
                    if (isSSA) {
                        sign.setParameter(new PSSParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA256, 32, 1));
                    }

                    sign.initVerify(sodFile.getDocSigningCertificate());
                    sign.update(sodFile.getEContent());
                    passiveAuthSuccess = sign.verify(sodFile.getEncryptedDigest());
                }
            } catch (Exception e) {
                Log.w(TAG, e);
//                setResult(ISO7816_READ_CARD_RES_ERROR_PASSIVE_AUTH);
//                finish();
            }
        }

        @Override
        protected Exception doInBackground(Void... params) {
            try {
                CardService cardService = CardService.getInstance(isoDep);
                cardService.open();

                service = new PassportService(cardService, NORMAL_MAX_TRANCEIVE_LENGTH, DEFAULT_MAX_BLOCKSIZE, false, false);
                service.open();

                boolean paceSucceeded = false;
                try {
                    CardAccessFile cardAccessFile = new CardAccessFile(service.getInputStream(PassportService.EF_CARD_ACCESS));
                    Collection<SecurityInfo> securityInfoCollection = cardAccessFile.getSecurityInfos();
                    for (SecurityInfo securityInfo : securityInfoCollection) {
                        if (securityInfo instanceof PACEInfo) {
                            PACEInfo paceInfo = (PACEInfo) securityInfo;
                            service.doPACE(bacKey, paceInfo.getObjectIdentifier(), PACEInfo.toParameterSpec(paceInfo.getParameterId()), null);
                            paceSucceeded = true;
                        }
                    }
                } catch (Exception e) {
                    Log.w(TAG, e);
                }

                service.sendSelectApplet(paceSucceeded);

                if (!paceSucceeded) {
                    try {
                        service.getInputStream(PassportService.EF_COM).read();
                    } catch (Exception e) {
                        service.doBAC(bacKey);
                    }
                }

                CardFileInputStream dg1In = service.getInputStream(PassportService.EF_DG1);
                dg1File = new DG1File(dg1In);

                CardFileInputStream dg2In = service.getInputStream(PassportService.EF_DG2);
                dg2File = new DG2File(dg2In);

                CardFileInputStream sodIn = service.getInputStream(PassportService.EF_SOD);
                sodFile = new SODFile(sodIn);

                // We perform Chip Authentication using Data Group 14
                doChipAuth(service);

                // Then Passive Authentication using SODFile
                doPassiveAuth();

                List<FaceImageInfo> allFaceImageInfo = new ArrayList<>();
                List<FaceInfo> faceInfos = dg2File.getFaceInfos();
                for (FaceInfo faceInfo : faceInfos) {
                    allFaceImageInfo.addAll(faceInfo.getFaceImageInfos());
                }

                if (!allFaceImageInfo.isEmpty()) {
                    FaceImageInfo faceImageInfo = allFaceImageInfo.iterator().next();

                    int imageLength = faceImageInfo.getImageLength();
                    DataInputStream dataInputStream = new DataInputStream(faceImageInfo.getImageInputStream());
                    byte[] buffer = new byte[imageLength];
                    dataInputStream.readFully(buffer, 0, imageLength);
                    InputStream inputStream = new ByteArrayInputStream(buffer, 0, imageLength);

                    Bitmap bitmap = ImageUtil.decodeImage(
                            activity, faceImageInfo.getMimeType(), inputStream);

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();

                    imageBase64 = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                }

            } catch (Exception e) {
                return e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Exception result) {
            if (result == null) {

                User user = new User();
                try {

                    CardBase64 cardBase64 = new CardBase64();

                    cardBase64.DG1 = Base64.encodeToString(dg1File.toString().getBytes(), Base64.DEFAULT);
                    cardBase64.DG2 = Base64.encodeToString(dg2File.toString().getBytes(), Base64.DEFAULT);
                    cardBase64.DG11 = Base64.encodeToString(new DG12File(service.getInputStream(PassportService.EF_DG12)).toString().getBytes(), Base64.DEFAULT);
                    cardBase64.DG12 = Base64.encodeToString(new DG12File(service.getInputStream(PassportService.EF_DG12)).toString().getBytes(), Base64.DEFAULT);
                    cardBase64.DG14 = Base64.encodeToString(dg14File.toString().getBytes(), Base64.DEFAULT);
                    cardBase64.DG15 = Base64.encodeToString(new DG15File(service.getInputStream(PassportService.EF_DG15)).toString().getBytes(), Base64.DEFAULT);
                    cardBase64.EF_COM = Base64.encodeToString(new COMFile(service.getInputStream(PassportService.EF_COM)).toString().getBytes(), Base64.DEFAULT);
                    cardBase64.EF_SOD = Base64.encodeToString(sodFile.toString().getBytes(), Base64.DEFAULT);

                    MRZInfo mrzInfo = dg1File.getMRZInfo();

                    user.firstName = mrzInfo.getSecondaryIdentifier();
                    user.lastName = mrzInfo.getPrimaryIdentifier();
                    user.dateOfBirth = mrzInfo.getDateOfBirth();
                    user.dateOfExpiry = mrzInfo.getDateOfExpiry();
                    user.documentNumber = mrzInfo.getDocumentNumber();
                    user.personalNumber = mrzInfo.getPersonalNumber();
                    user.gender = mrzInfo.getGender().toString();
                    user.issuingState = mrzInfo.getIssuingState();
                    user.nationality = mrzInfo.getNationality();
                    user.chipAuthSucceeded = chipAuthSucceeded;
                    user.passiveAuthSuccess = passiveAuthSuccess;
                    user.imageBase64 = imageBase64;

                    Card card = new Card(cardBase64, user);

                    Gson gson = new Gson();

//                    String jsonUser = gson.toJson(user);
//                    String cardBase64Json = gson.toJson(cardBase64);
//                    System.out.println(cardBase64Json);

                    String cardJson = gson.toJson(card);
                    Intent intent = new Intent();
                    intent.putExtra("card", cardJson);
                    setResult(ISO7816_READ_CARD_RES_OK, intent);

                } catch (IOException | CardServiceException e) {
                    e.printStackTrace();
                    setResult(ISO7816_READ_CARD_RES_ERROR_DATA_NOT_FOUND);
                }
            } else {
                setResult(ISO7816_READ_CARD_RES_ERROR_DATA_NOT_FOUND);
            }
            finish();
        }

    }

}

