package com.wedextim.e_passport;

import android.content.Intent;

import androidx.annotation.NonNull;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodChannel;

public class MainMethodResolver extends FlutterActivity {

    private static final String CHANNEL = "nfc_reader_activity";
    public static final String AA_CHALLENGE_METHOD = "aa_challenge_method";

    public static final int ISO7816_READ_CARD_RES_OK = 200;
    public static final int ISO7816_READ_CARD_REQUEST = 1;
    public static final int ISO7816_AA_CARD_CHALLENGE = 2;
    public static final int ISO7816_READ_CARD_RES_ERROR = 400;
    public static final int ISO7816_READ_CARD_RES_ERROR_MRZ_VALIDATE = 401;
    public static final int ISO7816_READ_CARD_RES_ERROR_CHIP_AUTH = 402;
    public static final int ISO7816_READ_CARD_RES_ERROR_PASSIVE_AUTH = 403;
    public static final int ISO7816_READ_CARD_RES_ERROR_DATA_NOT_FOUND = 404;
    public static final int ISO7816_READ_CARD_RES_ERROR_CERT_NOT_FOUND = 405;

    MethodChannel channel;
    MethodChannel.Result result;

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);
        channel = new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL);
        channel.setMethodCallHandler(
                (call, result) -> {
                    this.result = result;
                    System.out.println("call arguments" + call.arguments().toString());
                    if (call.method.equals(CHANNEL)) {
                        Intent intent = new Intent(this.getApplicationContext(), NFCReader.class);
                        intent.putExtra("method", CHANNEL);
                        intent.putExtra("passportNumber", call.argument("passportNumber").toString());
                        intent.putExtra("dateOfBirth", call.argument("dateOfBirth").toString());
                        intent.putExtra("dateOfExpiry", call.argument("dateOfExpiry").toString());
                        startActivityForResult(intent, ISO7816_READ_CARD_REQUEST);
                    } else if (call.method.equals(AA_CHALLENGE_METHOD)) {
                        Intent intent = new Intent(this.getApplicationContext(), NFCReader.class);
                        intent.putExtra("method", AA_CHALLENGE_METHOD);
                        intent.putExtra("challenge", call.argument("challenge").toString());
                        startActivityForResult(intent, ISO7816_AA_CARD_CHALLENGE);
                    }

                }
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }

        if (requestCode == ISO7816_READ_CARD_REQUEST) {
            if (resultCode == ISO7816_READ_CARD_RES_OK) {
                String cardJson = data.getStringExtra("card");
                result.success(cardJson);
            } else {
                result.error(String.valueOf(resultCode), String.valueOf(resultCode), null);
            }
        } else if (requestCode == ISO7816_AA_CARD_CHALLENGE) {
            if (resultCode == ISO7816_AA_CARD_CHALLENGE) {
                String challengeResult = data.getStringExtra("result");
                result.success(challengeResult);
            } else {
                result.error(String.valueOf(resultCode), String.valueOf(resultCode), null);
            }
        }

    }
}
