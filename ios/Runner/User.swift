public class User : Encodable {
    public var firstName : String = ""
    public var lastName : String = ""
    public var gender : String = ""
    public var issuingState: String = ""
    public var nationality : String = ""
    public var documentNumber : String = ""
    public var personalNumber : String = ""
    public var dateOfExpiry : String = ""
    public var dateOfBirth : String = ""
    public var passiveAuthSuccess : Bool = false
    public var chipAuthSucceeded : Bool = false
    public var imageBase64 : String?

    init() {
        self.imageBase64 = nil
    }
}
