import Combine
import NFCPassportReader
import UniformTypeIdentifiers

class NfcReader {
    
    var settings = SettingsStore()
    
    private var passportReader = PassportReader()
    private var flutterResult: FlutterResult
    
    var passportNumber: String = String()
    var dateOfBirth: String = String()
    var dateOfExpire: String = String()
    
    init(flutterResult: @escaping FlutterResult, pn: String, dob: String, doe:String) {
        self.flutterResult = flutterResult
        self.passportNumber = pn
        self.dateOfBirth = dob
        self.dateOfExpire = doe
    }
    
    func scan() {
        if #available(iOS 13.0.0, *) {
            self.scanPassport()
        } else {
            flutterResult(FlutterError(code: "400", message: "Unavailable version of IOS", details: nil))
        }
    }
}

extension NfcReader {
      
    func shareLogs() {
        PassportUtils.shareLogs()
    }

    func scanPassport( ) {
        
        var jsonData: String?
        
        let df = DateFormatter()
        df.timeZone = TimeZone(secondsFromGMT: 0)
        df.dateFormat = "YYYY-MM-dd"
        let dobDate = df.date(from: self.dateOfBirth)!
        let doeDate = df.date(from: self.dateOfExpire)!
        df.dateFormat = "YYMMdd"
        
        let _dob = df.string(from: dobDate)
        let _doe = df.string(from: doeDate)

        let passportUtils = PassportUtils()
        let mrzKey = passportUtils.getMRZKey( passportNumber: passportNumber, dateOfBirth: _dob, dateOfExpiry: _doe)

        // Set the masterListURL on the Passport Reader to allow auto passport verification
        let masterListURL = Bundle.main.url(forResource: "masterList", withExtension: ".pem")!
        passportReader.setMasterListURL( masterListURL )
        
        // Set whether to use the new Passive Authentication verification method (default true) or the old OpenSSL CMS verifiction
        passportReader.passiveAuthenticationUsesOpenSSL = !settings.useNewVerificationMethod
        // If we want to read only specific data groups we can using:
//        let dataGroups : [DataGroupId] = [.COM, .SOD, .DG1, .DG2, .DG7, .DG11, .DG12, .DG14, .DG15]
//        passportReader.readPassport(mrzKey: mrzKey, tags:dataGroups, completed: { (passport, error) in
        
        Log.logLevel = settings.logLevel
        Log.storeLogs = settings.shouldCaptureLogs
        Log.clearStoredLogs()
        
        let dataGroups : [DataGroupId] = [.COM, .SOD, .DG1, .DG2, .DG7, .DG11, .DG12, .DG14, .DG15]
        passportReader.readPassport(mrzKey: mrzKey, customDisplayMessage: { (displayMessage) in
            switch displayMessage {
                case .requestPresentPassport:
                    return "Hold your iPhone near an NFC enabled passport."
                default:
                    // Return nil for all other messages so we use the provided default
                    return nil
            }
        }, completed: { (passport, error) in
            if let passport = passport {
                // All good, we got a passport
                    // Save passport
                    let dict = passport.dumpPassportData(selectedDataGroups: DataGroupId.allCases, includeActiveAuthenticationData: true)
                    if let data = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted) {
                        jsonData = String(data: data, encoding: String.Encoding.utf8)!
                        if jsonData != nil {
                            self.flutterResult(String(jsonData!))
                        } else {
                            self.flutterResult(FlutterError(code: "400", message: "incorrect keys", details: nil))
                        }
                    }
                
                DispatchQueue.main.async {
                    self.settings.passport = passport
                }
            } else {
                self.flutterResult(FlutterError(code: "500", message: "Error: " + error.debugDescription, details: nil))
            }
        })
    }
    
    
    
    
    
    
//        @State private var showingAlert = false
//        @State private var showingSheet = false
//        @State private var showDetails = false
//        @State private var alertTitle : String = ""
//        @State private var alertMessage : String = ""
//        @State private var showSettings : Bool = false
//        @State private var showScanMRZ : Bool = false
//        @State private var showSavedPassports : Bool = false
//        var settings: SettingsStore
//
//        private let passportReader: PassportReader
//
//        init() {
//            passportReader = PassportReader()
//            settings = SettingsStore()
//        }
//
//        func scanPassport(pptNr: String, dob: String, doe:String ) {
//
//            let passportUtils = PassportUtils()
//
//            let df = DateFormatter()
//            df.timeZone = TimeZone(secondsFromGMT: 0)
//            df.dateFormat = "YYYY-MM-dd"
//            let dobDate = df.date(from: dob)!
//            let doeDate = df.date(from: doe)!
//            df.dateFormat = "YYMMdd"
//
//            let _dob = df.string(from: dobDate)
//            let _doe = df.string(from: doeDate)
//
//            let mrzKey = passportUtils.getMRZKey( passportNumber: pptNr, dateOfBirth: _dob, dateOfExpiry: _doe)
//            var jsonData: String?
//
//            // Set the masterListURL on the Passport Reader to allow auto passport verification
//            let masterListURL = Bundle.main.url(forResource: "masterList", withExtension: ".pem")!
//            passportReader.setMasterListURL( masterListURL )
//
//            // Set whether to use the new Passive Authentication verification method (default true) or the old OpenSSL CMS verifiction
//            passportReader.passiveAuthenticationUsesOpenSSL = !settings.useNewVerificationMethod
//
//            // If we want to read only specific data groups we can using:
//    //        let dataGroups : [DataGroupId] = [.COM, .SOD, .DG1, .DG2, .DG7, .DG11, .DG12, .DG14, .DG15]
//    //        passportReader.readPassport(mrzKey: mrzKey, tags:dataGroups, completed: { (passport, error) in
//
//            Log.logLevel = settings.logLevel
//            Log.storeLogs = settings.shouldCaptureLogs
//            Log.clearStoredLogs()
//
//    //        If we want to read only specific data groups we can using:
//    //        let dataGroups : [DataGroupId] = [.COM, .SOD, .DG1, .DG2, .DG7, .DG11, .DG12, .DG14, .DG15]
//    //        passportReader.readPassport(mrzKey: mrzKey, tags:dataGroups, completed: { (passport, error) in
//
//            // This is also how you can override the default messages displayed by the NFC View Controller
//            passportReader.readPassport(mrzKey: mrzKey, customDisplayMessage: { (displayMessage) in
//                switch displayMessage {
//                    case .requestPresentPassport:
//                        return "Hold your iPhone near an NFC enabled passport."
//                    default:
//                        // Return nil for all other messages so we use the provided default
//                        return nil
//                }
//            }, completed: { (passport, error) in
//                if let passport = passport {
//                    let dict = passport.dumpPassportData(selectedDataGroups: DataGroupId.allCases, includeActiveAuthenticationData: true)
//                    if let data = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted) {
//                        jsonData = String(data: data, encoding: String.Encoding.utf8)!
//                        if jsonData != nil {
//                            controller.dismiss(animated: true) {
//                                controller.navigationController?.popViewController(animated: true)
//                                result(String(jsonData!))
//                            }
//                        } else {
//                            controller.dismiss(animated: true) {
//                                controller.navigationController?.popViewController(animated: true)
//                                result(FlutterError(code: "400", message: "incorrect keys", details: nil))
//                            }
//                        }
//                    }
//                } else {
//                    controller.dismiss(animated: true) {
//                        controller.navigationController?.popViewController(animated: true)
//                        result(FlutterError(code: "500", message: "Error: " + error.debugDescription, details: nil))
//                    }
//                }
//            })
//        }
    }
