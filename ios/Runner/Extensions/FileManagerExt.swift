import SwiftUI

extension FileManager {
    static var cachesFolder : URL {
        FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
    }
}