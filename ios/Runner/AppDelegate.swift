import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    var controller : FlutterViewController?
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
        ) -> Bool {
        
        guard let flutterViewController  = window?.rootViewController as? FlutterViewController else {
            return super.application(application, didFinishLaunchingWithOptions: launchOptions)
        }
        
        GeneratedPluginRegistrant.register(with: flutterViewController.pluginRegistry())

        let flutterChannel = FlutterMethodChannel.init(name: "nfc_reader_activity", binaryMessenger: flutterViewController.binaryMessenger);
        flutterChannel.setMethodCallHandler { (flutterMethodCall, flutterResult) in
            if flutterMethodCall.method == "nfc_reader_activity" {
                if let args = flutterMethodCall.arguments as? Dictionary<String, Any>,
                    let passportNumber = args["passportNumber"]as? String,
                    let dateOfBirth = args["dateOfBirth"]as? String,
                    let dateOfExpiry = args["dateOfExpiry"]as? String {
                    let reader = NfcReader(
                        flutterResult: flutterResult, pn: passportNumber, dob: dateOfBirth, doe:dateOfExpiry
                    )
                    reader.scan()
                } else {
                    flutterResult(FlutterError(
                        code: "400", message: "Incorrect keys", details: nil
                    ))
                }
            }
        }
        
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    
}

