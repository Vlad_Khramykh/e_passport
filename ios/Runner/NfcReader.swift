import Combine
import NFCPassportReader
import UniformTypeIdentifiers

class NfcReader {
    
    var settings = SettingsStore()
    
    private var passportReader = PassportReader()
    private var flutterResult: FlutterResult
    
    var passportNumber: String = String()
    var dateOfBirth: String = String()
    var dateOfExpire: String = String()
    
    init(flutterResult: @escaping FlutterResult, pn: String, dob: String, doe:String) {
        self.flutterResult = flutterResult
        self.passportNumber = pn
        self.dateOfBirth = dob
        self.dateOfExpire = doe
    }
    
    func scan() {
        if #available(iOS 13.0.0, *) {
            self.scanPassport()
        } else {
            flutterResult(FlutterError(code: "400", message: "Unavailable version of IOS", details: nil))
        }
    }
}

extension NfcReader {
      
    func shareLogs() {
        PassportUtils.shareLogs()
    }

    func scanPassport( ) {
        
        let passportUtils = PassportUtils()
        
//        var jsonData: String?
        
        let df = DateFormatter()
        df.timeZone = TimeZone(secondsFromGMT: 0)
        df.dateFormat = "YYYY-MM-dd"
        let dobDate = df.date(from: self.dateOfBirth)!
        let doeDate = df.date(from: self.dateOfExpire)!
        df.dateFormat = "YYMMdd"
        let _dob = df.string(from: dobDate)
        let _doe = df.string(from: doeDate)
      
        let mrzKey = passportUtils.getMRZKey( passportNumber: passportNumber, dateOfBirth: _dob, dateOfExpiry: _doe)

        let masterListURL = Bundle.main.url(forResource: "masterList", withExtension: ".pem")!
        passportReader.setMasterListURL( masterListURL )
        
        passportReader.passiveAuthenticationUsesOpenSSL = !settings.useNewVerificationMethod
        
        Log.logLevel = settings.logLevel
        Log.storeLogs = settings.shouldCaptureLogs
        Log.clearStoredLogs()
        
        // This is also how you can override the default messages displayed by the NFC View Controller
        
        passportReader.readPassport(mrzKey: mrzKey, skipSecureElements: false,  customDisplayMessage: { (displayMessage) in
            switch displayMessage {
                case .requestPresentPassport:
                    return "Hold your iPhone near an NFC enabled passport."
                default:
                    // Return nil for all other messages so we use the provided default
                    return nil
            }
        }, completed: { (passport, error) in
            if let passport = passport {
 
                let dict = passport.dumpPassportData(selectedDataGroups: DataGroupId.allCases, includeActiveAuthenticationData: false)
                if let data = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted) {
                    
                    let card = Card()
                    card.cardBase64.DG1 = dict["DG1"]!
                    card.cardBase64.DG2 = dict["DG2"]!
                    card.cardBase64.DG11 = dict["DG11"]!
                    card.cardBase64.DG12 = dict["DG12"]!
                    card.cardBase64.DG14 = dict["DG14"]!
                    card.cardBase64.DG15 = dict["DG15"]!
                    card.cardBase64.EF_COM = dict["COM"]!
                    card.cardBase64.EF_SOD = dict["SOD"]!
                    
                    card.user.firstName = passport.firstName
                    card.user.lastName = passport.lastName
                    card.user.gender = passport.gender
                    card.user.issuingState = passport.issuingAuthority
                    card.user.nationality = passport.nationality
                    card.user.documentNumber = passport.documentNumber
                    card.user.personalNumber = passport.personalNumber
                    card.user.dateOfExpiry = passport.documentExpiryDate
                    card.user.dateOfBirth = passport.dateOfBirth
                    card.user.passiveAuthSuccess = passport.passportDataNotTampered
                    card.user.chipAuthSucceeded = passport.activeAuthenticationPassed
                    card.user.imageBase64 = passport.passportImage?.toString()                    
                
                    var json = String()
                    let jsonEncoder = JSONEncoder()
                
                    if let passportJson = try? jsonEncoder.encode(card) {
                        json = String(data: passportJson, encoding: String.Encoding.utf8)!
                        print(json)
                        self.flutterResult(String(json))
                    } else {
                        self.flutterResult(FlutterError(code: "400", message: "PassportDataModel is not parseble", details: nil))
                    }
                } else {
                    self.flutterResult(FlutterError(code: "400", message: "json data is null", details: nil))
                }
                
                DispatchQueue.main.async {
                    self.settings.passport = passport
                }
            } else {
                self.flutterResult(FlutterError(code: "401", message: "Error: " + error.debugDescription, details: nil))
            }
        })
    }
}

extension UIImage {
    func toString() -> String? {
        let data: Data? = self.jpegData(compressionQuality: .greatestFiniteMagnitude)
        return data?.base64EncodedString(options: .endLineWithLineFeed)
    }
}
